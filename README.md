# vue-taro-click-outside


### 介绍
> vue taro 小程序 元素外部点击事件插件，支持 typrscript 环境

### 安装教程
```bash
  npm i vue-taro-click-outside -S
```

### 使用教程

```ts
// vue taro 小程序工程 app.ts 入口文件
import Vue from 'vue';
import vueTaroClickOutside from "vue-taro-click-outside";

Vue.use(vueTaroClickOutside);
```
```html
<!--组件中使用-->
<div class="page" v-taro-click-outside="clickOutsideHandle"></div>
```
