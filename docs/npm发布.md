# npm 发布依赖说明

## registry
>如国内项目开发常用淘宝镜像仓库https://registry.npm.taobao.org/，但是npm发布时候需要调整远程仓库地址为 https://registry.npmjs.org/
```shell
npm config set registry https://registry.npmjs.org/

npm get registry
```

--------------------------------------------
## npm 登录
```shell
npm login
```
>按提示输入 npm账号、密码、邮箱、动态OTP口令等
![img_1.png](assets/npm_login.png)

--------------------------------------------
### npm 打包
```shell
npm pack
```
>本地项目根目录会生成“项目name-版本号.tgz"包文件

---------------------------------------------
## npm 发布
```shell
npm publish
```
>发布成功截图
![img.png](assets/npm_publish.png)
