// v-taro-click-outside导出指令的安装方法
import {vueTaroClickOutside} from "@/vue-taro-click-outside";

// vue 指令注册方法
const directiveInstall = (Vue: any): void  => {
    Vue.directives('v-taro-click-outside', vueTaroClickOutside);
}
// 指令安装插件
const directivePlugin: any = () => {};
// 指令安装插件绑定 install方法
directivePlugin.install = (Vue: any): void => {
    directiveInstall(Vue);
}
// 导出
export default directivePlugin;
