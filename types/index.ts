// types

export interface  IGetSelfElementsIdListOptions {
    el: any;
    currentNode: any;
}
export interface IGetTargetParentElementOptions  {
    el: any;
    currentNode: any;
    nodeName?: string;
    parentNodePropName?: string;
    parentNodePropValue?: string;
}

export declare type VUE_CLICK_OUTSIDE = {
    inserted?: Function;
    unbind?: Function;
}

// 导出指令安装方法
export declare function directivePlugin(): void;

